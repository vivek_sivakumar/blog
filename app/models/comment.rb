class Comment < ActiveRecord::Base
	belongs_to :post
		validate_presense_of :post_id
		validate_presense_of :body
end
