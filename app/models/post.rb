class Post < ActiveRecord::Base
	has_many :comments, dependent :destroy
	validate_presense_of :title
	validate_presense_of :body
end
